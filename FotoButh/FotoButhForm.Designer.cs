﻿using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System;
namespace FotoButh
{
    partial class FormFotoButh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxDisplay = new PictureBox();
            this.tableLayoutPanel1 = new TableLayoutPanel();
            this.panelConfig = new Panel();
            this.textBoxIp = new TextBox();
            this.label2 = new Label();
            this.textBoxComPort = new TextBox();
            this.label1 = new Label();
            this.panelMainDisplay = new Panel();
            this.panelStandby = new Panel();
            this.tableLayoutPanelQuad = new TableLayoutPanel();
            this.pictureBox4 = new PictureBox();
            this.pictureBox3 = new PictureBox();
            this.pictureBox2 = new PictureBox();
            this.pictureBox1 = new PictureBox();
            ((ISupportInitialize)this.pictureBoxDisplay).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelConfig.SuspendLayout();
            this.panelMainDisplay.SuspendLayout();
            this.panelStandby.SuspendLayout();
            this.tableLayoutPanelQuad.SuspendLayout();
            ((ISupportInitialize)this.pictureBox4).BeginInit();
            ((ISupportInitialize)this.pictureBox3).BeginInit();
            ((ISupportInitialize)this.pictureBox2).BeginInit();
            ((ISupportInitialize)this.pictureBox1).BeginInit();
            base.SuspendLayout();
            this.pictureBoxDisplay.Dock = DockStyle.Fill;
            this.pictureBoxDisplay.Location = new Point(0, 0);
            this.pictureBoxDisplay.Name = "pictureBoxDisplay";
            this.pictureBoxDisplay.Size = new Size(560, 294);
            this.pictureBoxDisplay.SizeMode = PictureBoxSizeMode.Zoom;
            this.pictureBoxDisplay.TabIndex = 1;
            this.pictureBoxDisplay.TabStop = false;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panelConfig, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelMainDisplay, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panelStandby, 0, 1);
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
            this.tableLayoutPanel1.Location = new Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
            this.tableLayoutPanel1.Size = new Size(772, 358);
            this.tableLayoutPanel1.TabIndex = 6;
            this.panelConfig.AutoSize = true;
            this.panelConfig.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.panelConfig.Controls.Add(this.textBoxIp);
            this.panelConfig.Controls.Add(this.label2);
            this.panelConfig.Controls.Add(this.textBoxComPort);
            this.panelConfig.Controls.Add(this.label1);
            this.panelConfig.Location = new Point(209, 3);
            this.panelConfig.Name = "panelConfig";
            this.panelConfig.Size = new Size(237, 52);
            this.panelConfig.TabIndex = 7;
            this.textBoxIp.Location = new Point(42, 29);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new Size(192, 20);
            this.textBoxIp.TabIndex = 4;
            this.textBoxIp.TextChanged += new EventHandler(this.textBoxIp_TextChanged);
            this.label2.AutoSize = true;
            this.label2.Location = new Point(19, 32);
            this.label2.Name = "label2";
            this.label2.Size = new Size(17, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "IP";
            this.textBoxComPort.Location = new Point(42, 3);
            this.textBoxComPort.Name = "textBoxComPort";
            this.textBoxComPort.Size = new Size(192, 20);
            this.textBoxComPort.TabIndex = 2;
            this.textBoxComPort.TextChanged += new EventHandler(this.textBoxComPort_TextChanged);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new Size(31, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "COM";
            this.panelMainDisplay.Controls.Add(this.pictureBoxDisplay);
            this.panelMainDisplay.Dock = DockStyle.Fill;
            this.panelMainDisplay.Location = new Point(209, 61);
            this.panelMainDisplay.Name = "panelMainDisplay";
            this.panelMainDisplay.Size = new Size(560, 294);
            this.panelMainDisplay.TabIndex = 9;
            this.panelStandby.Controls.Add(this.tableLayoutPanelQuad);
            this.panelStandby.Dock = DockStyle.Fill;
            this.panelStandby.Location = new Point(3, 61);
            this.panelStandby.Name = "panelStandby";
            this.panelStandby.Size = new Size(200, 294);
            this.panelStandby.TabIndex = 10;
            this.panelStandby.Visible = false;
            this.tableLayoutPanelQuad.ColumnCount = 2;
            this.tableLayoutPanelQuad.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
            this.tableLayoutPanelQuad.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
            this.tableLayoutPanelQuad.Controls.Add(this.pictureBox4, 1, 1);
            this.tableLayoutPanelQuad.Controls.Add(this.pictureBox3, 0, 1);
            this.tableLayoutPanelQuad.Controls.Add(this.pictureBox2, 1, 0);
            this.tableLayoutPanelQuad.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanelQuad.Dock = DockStyle.Fill;
            this.tableLayoutPanelQuad.Location = new Point(0, 0);
            this.tableLayoutPanelQuad.Name = "tableLayoutPanelQuad";
            this.tableLayoutPanelQuad.RowCount = 2;
            this.tableLayoutPanelQuad.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            this.tableLayoutPanelQuad.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            this.tableLayoutPanelQuad.Size = new Size(200, 294);
            this.tableLayoutPanelQuad.TabIndex = 8;
            this.pictureBox4.Dock = DockStyle.Fill;
            this.pictureBox4.Location = new Point(103, 150);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Size(94, 141);
            this.pictureBox4.SizeMode = PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox3.Dock = DockStyle.Fill;
            this.pictureBox3.Location = new Point(3, 150);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Size(94, 141);
            this.pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox2.Dock = DockStyle.Fill;
            this.pictureBox2.Location = new Point(103, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Size(94, 141);
            this.pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox1.Dock = DockStyle.Fill;
            this.pictureBox1.Location = new Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Size(94, 141);
            this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(772, 358);
            base.Controls.Add(this.tableLayoutPanel1);
            base.KeyPreview = true;
            base.Name = "FormFotoButh";
            this.Text = "Foto Buth";
            base.KeyPress += new KeyPressEventHandler(this.FormFotoButh_KeyPress);
            ((ISupportInitialize)this.pictureBoxDisplay).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panelConfig.ResumeLayout(false);
            this.panelConfig.PerformLayout();
            this.panelMainDisplay.ResumeLayout(false);
            this.panelStandby.ResumeLayout(false);
            this.tableLayoutPanelQuad.ResumeLayout(false);
            ((ISupportInitialize)this.pictureBox4).EndInit();
            ((ISupportInitialize)this.pictureBox3).EndInit();
            ((ISupportInitialize)this.pictureBox2).EndInit();
            ((ISupportInitialize)this.pictureBox1).EndInit();
            base.ResumeLayout(false);
        }

        #endregion

        private PictureBox pictureBoxDisplay;
        private TableLayoutPanel tableLayoutPanel1;
        private Panel panelConfig;
        private TextBox textBoxIp;
        private Label label2;
        private TextBox textBoxComPort;
        private Label label1;
        private Panel panelMainDisplay;
        private Panel panelStandby;
        private TableLayoutPanel tableLayoutPanelQuad;
        private PictureBox pictureBox4;
        private PictureBox pictureBox3;
        private PictureBox pictureBox2;
        private PictureBox pictureBox1;
    }
}

