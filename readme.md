#FotoButh

##Last Update
Years Ago

##Summary
Simple photo booth software which can be used with any camera that exposes a url to a jpeg of it's live image.

I originally wrote this software to be used in the photo booth at my wedding, which worked wonderfully.

I rigged up a button to a serial cable then used a serial to USB converter to ultimately plugin to the computer running the software. As you'll see from the following images the software looks to make sure the COM port you have specified actually has a device on it making everyone's life easier.

The software simply takes 4 jpeg images (after a countdown from 4) from a source via a HTTP get request to the camera sources jpeg image. The images' filenames reflect the time stamp and are then saved in an 'images' folder on the Desktop.

Pardon the onscreen keyboard, this was to take screenshots on my mac.

##Building
* Checkout the repository.
* Build with Visual Studio

##Screens
![](http://i.imgur.com/XHIcSm.jpg)
![](http://i.imgur.com/g927Fm.png)
![](http://i.imgur.com/s6hgim.png)
![](http://i.imgur.com/VH8Scm.png)
![](http://i.imgur.com/3WgNwm.png)
![](http://i.imgur.com/uyOTdm.png)